# aprende_jugando_juan
aprende_jugando_juan


Nombre del reto:   Aprende Jugando

En la actualidad los juegos virtuales se ha convertido en un gran pasatiempos para los jóvenes, estos permiten divertirse y pasar el tiempo desde la computadora y con la comodidad de estar en casa. Por tal 
motivo se creó JuegaYa! S.A. Una compañía que desea sistematizar su pool de juegos para que sus jugadores no pierdan la costumbre de tener esos juegos clásicos de infancia con solo un click.




A medida que se avanzo en el proyecto y luego de tomar en consideracion las recomendaciones por el tutor se han odtenido benos resultados en tanto a la estructura y el manejo de nuevas herramientas, a pesar que no esta finalizado en su totalidad este proyecto aun sigue en desarrollo faltando 2 puntos solicitados por el reto



Puntos a tener en cuenta al momento de iniciar sesion:
    -El rol admin se asigna en el \frontend\src\views\UpgradeToPro\UpgradeToPro.js  --> linea 108 modificando el numero asignado con el googleid que obtenga al logearse.
    -Cada vez que inicie sesion se ejecutara el metodo post almacenando la informacion y por defecto sera de rol usuario.
    -Para jugar al ahorcado se pulsan las letras en el teclado si las repite el notificara.
    -Correo adicional para iniciar sesión tanto como ADMIN y como USER 
        juand.20.contreras@gmail.com
        juand20contreras

    

Para el ejecutar el backend:

Se implementa arquitectura hexagonal para un rendimiento optimizado y se solucionan errores generales de la app

Para que se cree labase de datos elprograma mongod se debe estar ejecutando
Ejecutar los comando de instalacion:
    -pip install virtualenv    
    - virtualen venv    
    -pip install flask
    -pip install Flask-PyMongo
    -pip install flask-cors

    Requisitos:
        -Python 3.9.2
        -pip 21.1
        -Flask 1.0.2

    Para ejecutar, ingresar el comando:  python ./web.py


Para ejecutar el frontend:

requisitos: 
    -Node.js  v14.15.5
    -npx  6.14.11
comandos:
    -npm i react-router-dom bootswatch
    -npm install
    -npm run start



