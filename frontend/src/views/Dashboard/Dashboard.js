import React, {useState, useEffect} from "react";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import addGame from "components/Sumar/Sumar.js";

import { makeStyles } from "@material-ui/core/styles";
import Cookies from 'universal-cookie';
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";





const useStyles = makeStyles(styles);

const API = process.env.REACT_APP_API;


export default function Dashboard() {
  const classes = useStyles();
  const [user,setUser] = useState([]) 

  


  const getUser = async () => {
    const res = await fetch(`${API}/users/${cookies.get('id')}`)
    const data = await res.json();
    setUser(data)
    console.log(data)
  }

  useEffect(() => {
    getUser();

  }, [])
  
  console.log(user.ahorcado + 3)
  
  const cookies = new Cookies();
  cookies.set('rol', user.rol, {path: '/'});
  cookies.set('ahorcado', user.ahorcado, {path: '/'});
  cookies.set('triqui', user.triqui, {path: '/'});
  cookies.set('stop', user.stop, {path: '/'});
  
  

  return (

    <div className="Dashboard">

      <img src={cookies.get('image')}/>
      <h1>{cookies.get('name')}</h1>
      <h5>Email: {cookies.get('email')}</h5>
      
      
      <addGame/>
      
      
      


      <div className='col-md-6'>
      <table className="table table-striped">
        <thead>
          <tr>
                       
            <th>Ahorcado</th>
            <th>Triqui</th>
            <th>Stop</th>
            
          </tr>
        
        </thead>
        <tbody>
          
          
            <tr key={user._id}>
              
              <td>{user.ahorcado}</td>
              <td>{user.triqui}</td>
              <td>{user.stop}</td>             
              
            </tr>
          

        </tbody>
      </table>

    </div>

    
        
       

          

      </div>
    
  );
}
