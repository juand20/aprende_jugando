from flask import Flask, request, jsonify, render_template, url_for, session, redirect

from flask_pymongo import PyMongo, ObjectId
from hexagonal.adapters.database.mongoDB import database


db = database()

def create():
    if request.method == 'POST':
        user = database()
        existing_user = user.find_one({'email' : request.json['email']})
        
        if existing_user is None:
            id = db.insert({
                'name': request.json['name'],
                'email': request.json['email'],
                'image': request.json['image'],
                'ahorcado': request.json['ahorcado'],
                'triqui': request.json['triqui'],
                'stop': request.json['stop'],
                'password': request.json['password'],   
                'rol': request.json['rol']
                })    
            return jsonify(str(ObjectId(id)))

        if existing_user is not None:
            return  jsonify(str(existing_user['_id']))