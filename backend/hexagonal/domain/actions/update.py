from flask import Flask, request, jsonify, render_template, url_for, session, redirect

from flask_pymongo import PyMongo, ObjectId
from hexagonal.adapters.database.mongoDB import database


db = database()

def update(id):
    db.update_one({'_id': ObjectId(id)}, {'$set': {
        'name': request.json['name'],
        'email': request.json['email'],
        'image': request.json['image'],
        'ahorcado': request.json['ahorcado'],
        'triqui': request.json['triqui'],
        'stop': request.json['stop'],
        'password': request.json['password'],
        'rol': request.json['rol']
    }})

    return jsonify({'msg': 'User Updated'})