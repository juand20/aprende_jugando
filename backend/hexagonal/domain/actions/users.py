from flask import Flask, jsonify

from flask_pymongo import PyMongo, ObjectId
from hexagonal.adapters.database.mongoDB import database


db = database()


def users():
    users = []
    for doc in db.find():
        users.append({
            '_id': str(ObjectId(doc['_id'])),
            'name': doc['name'],
            'email': doc['email'],
            'image': doc['image'],
            'ahorcado': doc['ahorcado'],
            'triqui': doc['triqui'],
            'stop': doc['stop'],
            'password': doc['password'],
            'rol': doc['rol']
        })
    return jsonify(users)