from flask import Flask, request, jsonify, render_template, url_for, session, redirect

from flask_pymongo import PyMongo, ObjectId
from hexagonal.adapters.database.mongoDB import database


db = database()

def delete(id):
    db.delete_one({'_id' : ObjectId(id)})
    return  jsonify({'msg':'User delete'})