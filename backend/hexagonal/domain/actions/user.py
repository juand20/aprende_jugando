from flask import Flask, request, jsonify, render_template, url_for, session, redirect

from flask_pymongo import PyMongo, ObjectId
from hexagonal.adapters.database.mongoDB import database


db = database()

def user(id):
    user = db.find_one({'_id': ObjectId(id)})
    
    return  jsonify({
        '_id': str(ObjectId(user['_id'])),
        'name': user['name'],
        'email': user['email'],
        'image': user['image'],
        'ahorcado': user['ahorcado'],
        'triqui': user['triqui'],
        'stop': user['stop'],
        'password': user['password'],
        'rol': user['rol']
    })