from flask import Flask, request, jsonify, render_template, url_for, session, redirect
from hexagonal.adapters.database.mongoDB import database
from hexagonal.adapters.cors import conection

from hexagonal.domain.prueba import hello
from hexagonal.domain.actions.users import users
from hexagonal.domain.actions.create import create
from hexagonal.domain.actions.user import user
from hexagonal.domain.actions.delete import delete
from hexagonal.domain.actions.update import update



app = Flask(__name__)
conection(app)


db = database()


@app.route('/register', methods=['POST']) 
def createUser():
    res = create()
    return res
            

@app.route('/users', methods=['GET'])
def getUsers():
    res = users()
    return res
    
   

@app.route('/users/<id>', methods=['GET'])
def getUser(id):
    res=user(id)
    return res


@app.route('/users/<id>', methods=['DELETE'])
def deleteUser(id):
    res = delete(id)
    return res
    

@app.route('/users/<id>', methods=['PUT'])
def updateUser(id):
    res = update(id)
    return res

    
if __name__ == "__main__":
    app.run(debug=True)